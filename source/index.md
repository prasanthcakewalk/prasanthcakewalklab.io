# About

![headshot](assets/headshot.png){: style="height:160px;float:right;margin-left:30px;margin-bottom:10px"}

My name is Prasanth Shyamsundar. I am a postdoctoral research associate at the Fermilab Quantum Institute. My research revolves around quantum information science, machine learning, statistics and information theory, and high energy physics. I got my Ph.D. in physics from the University of Florida in August 2020.

My current academic aspiration is to work on personally exciting projects that have a meaningful and lasting impact on the methods employed in scientific research. My short-term professional goal is to figure out, work towards, and secure the next stage of my career, aka a (more) permanent job.

## Contact

Email: [prasanth@fnal.gov](mailto:prasanth@fnal.gov)
# Public Software

## ThickBrick

ThickBrick is a Python 3 implementation of certain data selection and categorization algorithms originally conceived in the context of data analysis in high energy physics.

The algorithms are intended to train event selectors and categorizers that maximize the sensitivity of physics analyses to the presence of a signal being searched for, or to the value of a parameter being measured.

Website: <https://prasanthcakewalk.gitlab.io/thickbrick/>  
Project repository: <https://gitlab.com/prasanthcakewalk/thickbrick/>

## RainDancesVI

RainDancesVI is TensorFlow-based implementation of Independent Classifier Networks (InClass nets) in Python 3. InClass nets can be used for

* Nonparametric estimation of conditional independence mixture models
* Unsupervised classification

Website: <https://prasanthcakewalk.gitlab.io/raindancesvi/>  
Project repository: <https://gitlab.com/prasanthcakewalk/raindancesvi/>
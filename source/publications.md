# Publications

A full list of my publications can be found here:  

* INSPIRE-HEP: [https://inspirehep.net/literature?sort=mostrecent&size=25&page=1&q=a%20P.Shyamsundar.1](https://inspirehep.net/literature?sort=mostrecent&size=25&page=1&q=a%20P.Shyamsundar.1)  
* arXiv: [https://arxiv.org/search/?query=Shyamsundar%2C+Prasanth&searchtype=author](https://arxiv.org/search/?query=Shyamsundar%2C+Prasanth&searchtype=author)

## Selected publications

1. _"Deep-Learned Event Variables for Collider Phenomenology"_  
  Doojin Kim, Kyoungchul Kong, Konstantin T. Matchev, Myeonghun Park, Prasanth Shyamsundar  
  [arXiv:2105.10126 \[hep-ph\]](https://arxiv.org/abs/2105.10126)
* _"Non-Boolean Quantum Amplitude Amplification and Quantum Mean Estimation"_  
  Prasanth Shyamsundar  
  [arXiv: 2102.04975 \[quant-ph\]](https://arxiv.org/abs/2102.04975)
* _"InClass Nets: Independent Classifier Networks for Nonparametric Estimation of Conditional Independence Mixture Models and Unsupervised Classification"_  
  Konstantin T. Matchev, Prasanth Shyamsundar  
  [arXiv:2009.00131 \[stat.ML\]](https://arxiv.org/abs/2009.00131)
* _"OASIS: Optimal Analysis-Specific Importance Sampling for event generation"_  
  Konstantin T. Matchev, Prasanth Shyamsundar  
  [arXiv:2006.16972 \[hep-ph\]](https://arxiv.org/abs/2006.16972) &emsp;&emsp; [SciPost Phys. **10** (2021) 2, 034](https://doi.org/10.21468/SciPostPhys.10.2.034)
	<!--* _"Finding Wombling Boundaries in LHC Data with Voronoi and Delaunay Tessellations"_  -->
	<!--  Konstantin T. Matchev, Alex Roman, Prasanth Shyamsundar  -->
	<!--  [arXiv:2006.06582 \[hep-ph\]](https://arxiv.org/abs/2006.06582)-->
	<!--* _"A quantum algorithm for model independent searches for new physics"_  -->
	<!--  Konstantin T. Matchev, Prasanth Shyamsundar, Jordan Smolinsky  -->
	<!--  [arXiv:2003.02181 \[hep-ph\]](https://arxiv.org/abs/2003.02181)-->
	<!--* _"Uncertainties associated with GAN-generated datasets in high energy physics"_  -->
	<!--  Konstantin T. Matchev, Prasanth Shyamsundar  -->
	<!--  [arXiv:2002.06307 \[hep-ph\]](https://arxiv.org/abs/2002.06307)-->
* _"Optimal event selection and categorization in high energy physics, Part 1: Signal discovery"_  
  Konstantin T. Matchev, Prasanth Shyamsundar  
  [arXiv:1911.12299 \[physics.data-an\]](https://arxiv.org/abs/1911.12299) &emsp;&emsp; [J. High Energ. Phys. **03** (2021), 291](https://doi.org/10.1007/JHEP03(2021)291)
* _"Singularity Variables for Missing Energy Event Kinematics"_  
  Konstantin T. Matchev, Prasanth Shyamsundar  
  [arXiv:1911.01913 \[hep-ph\]](https://arxiv.org/abs/1911.01913) &emsp;&emsp; [J. High Energ. Phys. **04** (2020), 027](https://doi.org/10.1007/JHEP04(2020)027)
* _"Kinematic Focus Point Method for Particle Mass Measurements in Missing Energy Events"_  
  Doojin Kim, Konstantin T. Matchev, Prasanth Shyamsundar  
  [arXiv:1906.02821 \[hep-ph\]](https://arxiv.org/abs/1906.02821) &emsp;&emsp; [J. High Energ. Phys. **10** (2019), 154](https://doi.org/10.1007/JHEP10(2019)154)
	<!--* _"Measuring the mass, width, and couplings of semi-invisible resonances with the Matrix Element Method"_  -->
	<!--  Amalia Betancur, Dipsikha Debnath, James S. Gainer, Konstantin T. Matchev, Prasanth Shyamsundar  -->
	<!--  [arXiv:1708.07641 \[hep-ph\]](https://arxiv.org/abs/1708.07641) &emsp;&emsp; [Phys. Rev. D **99** (2019) 11, 116007](https://doi.org/10.1103/PhysRevD.99.116007)-->